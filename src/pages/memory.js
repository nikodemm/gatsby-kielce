import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';
import SEO from '../components/seo';
import MemoryGame from '../components/memoryGame.js';

const memoryPage = () => {
    return ( 
        <Layout>
            <SEO title="Gra pamięć"/>
            <h1>Gra pamięciowa :)</h1>
            <Link to="/">Wróć na stronę główną</Link>            
            <MemoryGame /> 
        </Layout>
    );
}

export default memoryPage;  