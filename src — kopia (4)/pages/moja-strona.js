import { Link } from 'gatsby';
import React from 'react';  
import { MyComponent2 } from '../components/myComponent2';
import Layout from '../components/layout';
import Title from '../components/title';   //ważne gdzie jesteśmy - dobrze użyć autoimportu

import SEO from '../components/seo';const arrayOfObjects = [{
    id: 1,
    title: 'Title 1',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 2,
    title: 'Title 2',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 3,
    title: 'Title 3',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 4,
    title: 'Title 4',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 5,
    title: 'Title 5',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}];

//SSR=>Server Side Renfering => Next.js
//SSG => Static Site Generator = > Gatsby.js = React + Routing

class MojaStronaPage extends React.Component {    componentDidMount() {
    }    componentWillUnmount() {
        console.log('tu component will unmount!');
    }    
    
    render() {
        return (
            <Layout>
                <SEO title="Moja strona!s" />              
                <h1>Moja strona</h1>
                <Link to="/">Wroc na strone glowna</Link>
                <Title 
                   title="tytuł 11"
                   subtitle="podtytuł1" 
                />
                {arrayOfObjects.map((object, key) => (
                    <MyComponent2 key={key} {...object} />
                ))}
                <Title 
                   title="tytuł 22"
                   subtitle="podtytuł 2" 
                />
                <Title 
                   title="tytuł 33"          //tu nawiasy powinny, ale nie muszą być, bo to zwykły ciąg znaków  
                   subtitle={"podtytuł 3"}   //tu nawiasy powinny, ale nie muszą być, bo to zwykły ciąg znaków
                   changeHeaderColor={true}
                />
                <Title 
                   title="tytuł 44"
                   subtitle="podtytuł 4" 
                />

            </Layout>
        );
    }    
}

export default MojaStronaPage;