import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const StyledWrapper = styled.div`
    padding: 100px;
    background-color: ${({theme}) => theme.colors.mainGreen};
    border: 3px solid #e4e4e4;
    @media(max-width: 600px) {
        padding: 30 prefix;
    }

`;

const StyledHeader = styled.h1`
    background-color: ${({changeHeaderColor}) => changeHeaderColor ? 'aquamarine' : 'red'};
    height: ${({height}) => height}px;
    span {
        font-weight:bold;
        color: purple;
    }
    color:blue;
`;

const StyledSubHeader = styled.h2`
    background-color:green;
    color:red;
    &.bold {   //1 metoda
        font-weight: 300;
    }
    
`;

const StyledSubHeaderWithBorder = styled(StyledSubHeader)`
    border: 2px solid pink;
`

const Title = ({title, subtitle, changeHeaderColor}) => {
    return (
        <StyledWrapper>
            <StyledHeader 
                changeHeaderColor={changeHeaderColor}
                height={changeHeaderColor ? 500 : 300}    
            >
                <span>1.</span>{title}
            </StyledHeader>
            <StyledSubHeader>{subtitle}</StyledSubHeader>
            <StyledSubHeader className="bold">{subtitle}</StyledSubHeader>
            <StyledSubHeaderWithBorder>{subtitle}</StyledSubHeaderWithBorder>
        </StyledWrapper>
      );
}

Title.defaultProps = {
    title: 'Uzupełnij Tytuł',
    subtitle: 'Uzupełnij podTytuł',  
    changeHeaderColor: false,   

}

Title.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    changeHeaderColor: PropTypes.bool,
}

export default Title;