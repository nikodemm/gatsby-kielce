import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import Button from '@material-ui/core/Button';
// LUB tak: import { Button } from '@material-ui/core/Button';

import P01IMG from '../images/memory/01.png';
import P02IMG from '../images/memory/02.png';
import P03IMG from '../images/memory/03.png';
import P04IMG from '../images/memory/04.png';
import P05IMG from '../images/memory/05.png';
import P06IMG from '../images/memory/06.png';
import P07IMG from '../images/memory/07.png';
import P08IMG from '../images/memory/08.png';

const StyledWrapper = styled.div`
    border: 1px solid red;
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
`;
const StyledCard = styled.button`
    border: 1px solid #000;
    width: 25%;
    background-color: #fff;
    height: 200px;
    outline: none;
    overflow: hidden;    img {
        margin: 0;
        transition: all 0.5s ease;
        opacity: ${({active}) => active ? '1' : '0'};
        transform: ${({active}) => active 
            ? 'scale(1) rotate(360deg)' 
            : 'scale(0) rotate(0deg)'
        };
        max-width: 100%;
        max-height: 100%;
    }

    
    /* &.active {
        img {
            display: block;
        }
    } */
`;

const imagesDefinitions = [
{
    key: 1,
    src: P01IMG,
}, {
    key: 2,
    src: P02IMG,
}, {
    key: 3,
    src: P03IMG,
}, {
    key: 4,
    src: P04IMG,
}, {
    key: 5,
    src: P05IMG,
}, {
    key: 6,
    src: P06IMG,
}, {
    key: 7,
    src: P07IMG,
}, {
    key: 8,
    src: P08IMG,
},     
];


function App() {
    return <Button color="primary">Hello World</Button>;
}


const MemoryGame = () => {
    const [moveNumber, setMoveNumber] = useState(1);
    const [images, setImages] = useState([]);
    const [clickedImages, setClickedImages] = useState([]);
    const [guessedKeys, setGuessedKeys] = useState([]);   
    
    useEffect(() => {
        setupImages();
    }, []);

    useEffect(() => {
        if(clickedImages.length !== 2) {
            return;
        }
        const [firstImageIndex, secondImageIndex] = clickedImages;
        const firstImageKey = images[firstImageIndex].key;
        const secondImageKey = images[secondImageIndex].key;
        // console.log({firstImageKey, secondImageKey});
        let timeoutTime = 1500;
        if(firstImageKey === secondImageKey) {
            timeoutTime = 0;
            setGuessedKeys([...guessedKeys, firstImageKey])
        }
        setClickedImages([]);
        
        setTimeout(() => {
            setMoveNumber(moveNumber + 1); 
            setClickedImages([]);
        }, timeoutTime);
    
    }, [clickedImages]);
    
    const setupImages = () => {
        const imagesArray = [...imagesDefinitions, ...imagesDefinitions];
        for(let i = imagesArray.length - 1; i > 0; i--){
            const j = Math.floor(Math.random() * i)
            const temp = imagesArray[i]
            imagesArray[i] = imagesArray[j]
            imagesArray[j] = temp
        }
        setImages(imagesArray);
    } 
    
    const isImageVisible = (index) => {
        const isImageClicked = clickedImages.indexOf(index) > -1;
        const key = images[index].key;  //tak pobieramy image
        // const {key} = images[index];  //tak pobieramy image
        const isImageGuessed = guessedKeys.indexOf(key) > -1;
        return isImageClicked || isImageGuessed;
    }

    const onClick = (index) => {
        if(isImageVisible(index) || clickedImages.length > 1) {
            return;
        }
        setClickedImages([...clickedImages, index]);
    }

    const onReset = () => {
        setupImages();
        setMoveNumber(1);
        setClickedImages([]);
        setGuessedKeys([]);
    }

    const isResetVisible = guessedKeys.length === imagesDefinitions.length;

    return (<>
        {/* <AppBar position="static">
        <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
                News
            </Typography>
            <Button color="inherit">Login</Button>
        </Toolbar>
        </AppBar> */}

        <h1>Memory Game</h1>
        <Button 
            variant="contained"  
            color="primary">Goodbye World!
        </Button>
        {!isResetVisible && (<h2>Ruch: {moveNumber}</h2>)}
        {isResetVisible && (
            <h3>Gra skończona! 
                <button 
                    onClick={onReset}>Uruchom ponownie
                </button>
            </h3>
        )}
        {images.map((image, key) => (
            <img key={key} src={image.src} />
        ))}
        <StyledWrapper>
            {images.map((image, index) => (
                <StyledCard 
                    key={index} 
                    active={isImageVisible(index)}
                    onClick={() => onClick(index)}
                >
                    <img src={image.src} />
                </StyledCard>
            ))}
        </StyledWrapper>
    </>)
}
export default MemoryGame;