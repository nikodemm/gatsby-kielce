import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

//JSX

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Haj pipul</h1>
    <p>Witamy w Twoich nowych Gaciach.</p>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/moja-strona/">Goł tu moja dobra strona</Link> <br />
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link> <br />
    <Link to="/kolko-i-krzyzyk/">Idź do kólka i krzyżyka</Link> <br />
    <Link to="/memory/">Idź do gry pamięciowej</Link> <br />
  </Layout>
)

export default IndexPage
