import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import P01IMG from '../images/memory/01.png';
import P02IMG from '../images/memory/02.png';
import P03IMG from '../images/memory/03.png';
import P04IMG from '../images/memory/04.png';
import P05IMG from '../images/memory/05.png';
import P06IMG from '../images/memory/06.png';
import P07IMG from '../images/memory/07.png';
import P08IMG from '../images/memory/08.png';

const StyledWrapper = styled.div`
    border: 1px solid red;
    padding: 20px;
`;
const StyledCard = styled.button`
    border: 1px solid #000;
    height: 100px;
    width: 100px;
    background-color: #eee;
`;

const imagesDefinitions = [
    P01IMG,
    P02IMG,
    P03IMG,
    P04IMG,
    P05IMG,
    P06IMG,
    P07IMG,
    P08IMG,
];

const MemoryGame = () => {
    const [images, setImages] = useState([]);    useEffect(() => {
        setupImages();
    }, []);
    
    const setupImages = () => {
        setImages([...imagesDefinitions, ...imagesDefinitions]);
    } 
    
    return (<>
        <h1>Memory Game</h1>
        {images.map((image, key) => (
            <img key={key} src={image} />
        ))}
        <StyledWrapper>
            <StyledCard />
            <StyledCard />
            <StyledCard />
            <StyledCard />
            <StyledCard />
            <StyledCard />
            <StyledCard />
        </StyledWrapper>
    </>)
}
export default MemoryGame;